**Project Drawdown Climate Solution Models**

https://www.drawdown.org/about

Project Drawdown gathers and facilitates a broad coalition of researchers,
scientists, graduate students, PhDs, post-docs, policy makers, business
leaders and activists to assemble and present the best available
information on climate solutions in order to describe their beneficial
financial, social and environmental impact over the next thirty years.

To date, the full range and beneficial impact of climate solutions have not
been explained in a way that bridges the divide between urgency and agency.
The aspirations of people who want to enact meaningful solutions remain
largely untapped. Dr. Leon Clark, one of the lead authors of the IPCC 5th
Assessment, wrote, "We have the technologies, but we really have no sense
of what it would take to deploy them at scale." Together, let’s figure this
out.

Project Drawdown was founded by author, entrepreneur, and environmentalist
Paul Hawken in 2014 to map, measure, and model the most substantive solutions
to stop global warming, and to communicate those findings to the world.
